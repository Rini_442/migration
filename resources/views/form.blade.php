<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat account baru</h1>
<h3>Sign Up Form</h3><br>
<form action="/post" method="POST">
@csrf
<label>First Name</label><br>
<input type="text" name="firstname"><br>
<label>Last Name</label><br>
<input type="text" name="lastname"><br><br>
<label> Gender</label> <br><br>
        <input type="radio" name="Gender"> Male <br><br>
        <input type="radio" name="Gender"> Female <br><br>
        <input type="radio" name="Gender"> Other <br><br>
        <label> Nationality</label> <br><br>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="South Korea">South Korea</option>
            <option value="Palestina">Palestina</option>
        </select> <br><br>
        <label> Language Spoken</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia<br><br>
        <input type="checkbox">English<br><br>
        <input type="checkbox">Other <br><br>
        <label> Bio</label> <br><br>
    <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>
<input type="submit" value="Kirim">
</form>
</body>
</html>


