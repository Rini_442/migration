@extends('layout.master')

@section('title')
Tambah cast
@endsection 

@section('content')

<form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">nama cast</label>
                <input type="text" class="form-control" name="namacast" id="nama" placeholder="Masukkan nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group">
                <label for="umur">umur cast</label>
                <input type="text" class="form-control" name="umurcast" id="umur" placeholder="Masukkan umur cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bio">bio cast</label>
                <input type="text" class="form-control" name="biocast" id="bio" placeholder="Masukkan bio cast">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection